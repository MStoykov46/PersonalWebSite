import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { ArtsService } from './services/arts.service';
import { AuthService } from './services/authService.service';
import { DialogService } from './services/dialog.service';
import { StorageService } from './services/storage.service';



@NgModule({
  declarations: [],
  imports: [
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  providers: [
    AuthService,
    StorageService,
    DialogService,
    ArtsService,
  ]
})
export class CoreModule { }
