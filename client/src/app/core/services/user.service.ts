import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserDTO } from '../../components/models/User.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  getUserById(id: string) {
    return this.http.get<UserDTO>(`http://localhost:5000/api/User/${id}`);
  }

  deleteUser(id: string) {
    return this.http.delete<UserDTO>(`http://localhost:5000/api/User/${id}`);
  }
}
