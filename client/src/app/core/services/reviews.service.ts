import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReviewDTO } from '../../features/reviews/models/review.dto';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public createReview(artItemId: string, userId: string, content: string ) {
    return this.http.post<ReviewDTO>(`http://localhost:5000/api/comments`, { artItemId, userId, content });
  }

  public deleteReview(id: string): Observable<string> {
    return this.http.delete<string>(`http://localhost:5000/api/comments/${id}`);
  }

  public updateReview(id: string, newText: string) {
    return this.http.put<ReviewDTO>(`http://localhost:5000/api/comments/${id}`, { content: newText });
  }

  public getArtReviews(artId: string) {
    return this.http.get<ReviewDTO[]>(`http://localhost:5000/api/art/${artId}/comments`);
  }
}
