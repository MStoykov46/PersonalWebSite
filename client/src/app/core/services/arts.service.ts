import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArtItemDTO } from '../../features/art-items/models/artItem.dto';
import { CreateArtItemDTO } from '../../features/art-items/models/createArtItem.dto';

@Injectable({
  providedIn: 'root'
})
export class ArtsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllArts() {
    return this.http.get<ArtItemDTO[]>(`http://localhost:5000/api/arts`);
  }
  public getAllArtsByUser(userId: string) {
    return this.http.get<ArtItemDTO[]>(`http://localhost:5000/api/user/${userId}/arts`);
  }

  public getSingleArt(id: string): Observable<ArtItemDTO> {
    return this.http.get<ArtItemDTO>(`http://localhost:5000/api/art/${id}`);
  }

  public createArt(art: CreateArtItemDTO): Observable<ArtItemDTO> {
    const formData = new FormData();
    console.log(art);

    formData.append('description', art.description);
    formData.append('image', art.image);
    formData.append('userId', art.userId);
    console.log(formData);

    return this.http.post<ArtItemDTO>(`http://localhost:5000/api/art`, formData, { headers: { Accept: '*/*' }});
  }
}
