import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtPayloadDTO } from '../../components/models/JwtPayload.dto';
import { LoginDTO } from '../../components/models/LoginUser.dto';
import { RegisterDTO } from '../../components/models/Register.dto';
import { UserDTO } from '../../components/models/User.dto';
import { StorageService } from './storage.service';


@Injectable()
export class AuthService {

  private readonly isLoggedSubject$: BehaviorSubject<boolean> = new BehaviorSubject(this.isUserLoggedIn());
  private readonly loggedUserSubject$: BehaviorSubject<JwtPayloadDTO> = new BehaviorSubject(this.loggedUser());


  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly helper: JwtHelperService,
    private readonly router: Router,
  ) {}

  public login(user: LoginDTO) {
    const logInUser: LoginDTO = { username: user.username, password: user.password };
    return this.http.post<{ token: string }>(`http://localhost:5000/api/User/login`, logInUser)
      .pipe(
        tap(
          (data) => {
              const loggedUser: JwtPayloadDTO = JSON.parse(this.helper.decodeToken(data.token).user);
              console.log(loggedUser);

              const storageType: string = user.keepMeLoggedIn ? 'local' : 'session';

              this.storageService.setItem('token', data.token, storageType);

              this.isLoggedSubject$.next(true);
              this.loggedUserSubject$.next(loggedUser);
          },
          (error) => {
            // fail silently
          }
        )
      );
  }

  public logout() {
    this.router.navigateByUrl('/home');
    this.storageService.delete('token');
    this.isLoggedSubject$.next(false);
    this.loggedUserSubject$.next(null);
  }

  public register(user: RegisterDTO): Observable<UserDTO> {
    return this.http.post<UserDTO>('http://localhost:5000/api/User', user);
  }

  private isUserLoggedIn() {
    const token = this.storageService.getItem('token');
    return !!token;
  }

  public get isLogged$(): Observable<boolean> {
    return this.isLoggedSubject$.asObservable();
  }

  public get loggedUser$(): Observable<JwtPayloadDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public getDecodedAccessToken(token: string) {
    try {
      return this.helper.decodeToken(token);
    } catch (error) {
      // silent error
    }
  }

  private loggedUser(): JwtPayloadDTO {
    try {
      return JSON.parse(this.helper.decodeToken(this.storageService.getItem('token')).user);
    } catch (error) {
      // in case of StorageService tampering
      this.isLoggedSubject$.next(false);

      return null;
    }
  }
}
