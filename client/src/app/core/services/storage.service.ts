import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  public setItem(key: string, value: string, storageType: string) {
    value = JSON.stringify(value);

    storageType === 'local' ?
      localStorage.setItem(key, value) : sessionStorage.setItem(key, value);
  }

  public getItem(key: string) {
    const localStorageToken = localStorage.getItem(key);
    const value = localStorageToken ? localStorageToken : sessionStorage.getItem(key);

    return value ? JSON.parse(value) : null;
  }

  public delete(key: string) {
    sessionStorage.removeItem(key);
    localStorage.removeItem(key);
  }

  public clear() {
    sessionStorage.clear();
    localStorage.clear();
  }
}
