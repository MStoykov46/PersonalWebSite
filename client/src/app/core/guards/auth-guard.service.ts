import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../services/authService.service';
import { NotificationService } from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private router: Router,
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.authService.isLogged$
        .pipe(
          tap(loggedIn => {

            if (!loggedIn) {
              this.notificationService.error('Please log in first!');
              this.router.navigate(['/home']);
            }
          }),
        );

  }

}
