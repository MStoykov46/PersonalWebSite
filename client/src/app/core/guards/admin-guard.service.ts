import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtPayloadDTO } from '../../components/models/JwtPayload.dto';
import { AuthService } from '../services/authService.service';
import { NotificationService } from '../services/notification.service';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard {

  constructor(
    private authService: AuthService,
    private router: Router,
    private storageService: StorageService,
    private readonly notificationService: NotificationService,
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token: string = this.storageService.getItem('token');

    if (!token) {
      this.router.navigate(['/not-found']);
      return false;
    }
    const user: JwtPayloadDTO = this.authService.getDecodedAccessToken(token);

    if (!user || user.role.name !== next.data.role) {
      this.router.navigate(['/not-found']);
      return false;
    }

    return true;
  }

}
