import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/authService.service';
import { StorageService } from '../services/storage.service';

@Injectable()
export class AttachTokenInterceptor implements HttpInterceptor {

  constructor(
    private readonly storageService: StorageService,
    private readonly authService: AuthService,
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const token = this.storageService.getItem('token');

    req = req.clone({
        setHeaders: {
          authorization: `Bearer ${token}`,
        }
    });

    console.log(req);


    return next.handle(req);
  }
}
