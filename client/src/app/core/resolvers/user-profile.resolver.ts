import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserDTO } from '../../components/models/User.dto';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class UserProfileResolver implements Resolve<UserDTO> {
  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
  ): Observable<UserDTO> {
    const id = route.paramMap.get('id');
    return this.userService.getUserById(id)
      .pipe(
        map((user: UserDTO) => {
          if (user) {
            return user;
          } else {
            this.router.navigate(['/']);
            return;
          }
        })
      );
  }
}
