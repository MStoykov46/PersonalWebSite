import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JwtPayloadDTO } from '../../components/models/JwtPayload.dto';
import { ArtItemDTO } from '../../features/art-items/models/artItem.dto';
import { ArtsService } from '../services/arts.service';
import { AuthService } from '../services/authService.service';

@Injectable({
  providedIn: 'root'
})
export class AllUserArtsResolver implements Resolve<ArtItemDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly artService: ArtsService,
    private readonly authService: AuthService,
  ) { }

  resolve(): Observable<ArtItemDTO[]> {
    let user: JwtPayloadDTO;

    this.authService.loggedUser$.subscribe(
      (loggedUser: JwtPayloadDTO) => {
        user = loggedUser;
      }
    );

    return this.artService.getAllArtsByUser(user.Id)
      .pipe(
        map((art: ArtItemDTO[]) => {
          if (art) {
            return art;
          } else {
            this.router.navigate(['/']);
            return;
          }
        })
      );
  }
}
