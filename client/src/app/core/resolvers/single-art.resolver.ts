import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArtItemDTO } from '../../features/art-items/models/artItem.dto';
import { ArtsService } from '../services/arts.service';

@Injectable({
  providedIn: 'root'
})
export class SingleArtResolver implements Resolve<ArtItemDTO> {

  constructor(
    private readonly router: Router,
    private readonly artService: ArtsService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
  ): Observable<ArtItemDTO> {
    const id = route.paramMap.get('id');
    return this.artService.getSingleArt(id)
      .pipe(
        map((art: ArtItemDTO) => {
          if (art) {
            return art;
          } else {
            this.router.navigate(['/']);
            return;
          }
        })
      );
  }
}
