import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-server-error-page',
  templateUrl: './server-error-page.component.html',
  styleUrls: ['./server-error-page.component.css']
})
export class ServerErrorPageComponent implements OnInit {


  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit() {}

  public redirectToHomePage() {
    this.router.navigateByUrl('/home');
  }
}
