import { RoleDTO } from './Role.dto';

export class UserDTO {
  id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  roleId: string;
  role: RoleDTO;
  createTime: string;
}
