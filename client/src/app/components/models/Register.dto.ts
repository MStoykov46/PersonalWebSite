export class RegisterDTO {
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}
