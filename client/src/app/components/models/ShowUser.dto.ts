export class ShowUserDTO {

  public id: string;

  public username: string;

  public email: string;

  public role: string;

}

