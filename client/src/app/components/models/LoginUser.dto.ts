export class LoginDTO {
  public username: string;
  public password: string;
  public keepMeLoggedIn?: boolean;
}
