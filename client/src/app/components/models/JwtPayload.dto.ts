import { RoleDTO } from './Role.dto';

export class JwtPayloadDTO {
  Id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  role: RoleDTO;
  createTime: string;
}
