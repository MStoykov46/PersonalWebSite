import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/authService.service';
import { NotificationService } from '../../core/services/notification.service';
import { LoginDTO } from '../models/LoginUser.dto';
import { RegisterDTO } from '../models/Register.dto';
import { UserDTO } from '../models/User.dto';

@Component({
  selector: 'app-landing-dialog',
  templateUrl: './landing-dialog.component.html',
  styleUrls: ['./landing-dialog.component.scss']
})
export class LandingDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<LandingDialogComponent>,
    public authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) {}

  ngOnInit() {}

  public login(event: LoginDTO) {
    console.log(event);
    this.authService.login(event).subscribe(
      token => {
        this.dialogRef.close(true);
      },
      error => {
        if (error.error === 'Password does not match') {
          this.notificationService.error(error.error);
        } else {
          this.notificationService.error('Unknown error occured');
          this.dialogRef.close(false);
          this.router.navigateByUrl('/server-error');
        }
        // notification , stay on dialog
      }
    );
  }

  public register(event: RegisterDTO) {
    console.log(event);
    this.authService.register(event).subscribe(
      (user: UserDTO) => {
        this.dialogRef.close(true);
        this.router.navigateByUrl('/home');
      },
      (error) => {
        this.notificationService.warn('Unknown error occured');
        this.router.navigateByUrl('/server-error');
      }
    );
  }

  public onCancelClick() {
    this.dialogRef.close(false);
  }
}
