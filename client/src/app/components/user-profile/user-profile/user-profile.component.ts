import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/services/authService.service';
import { NotificationService } from '../../../core/services/notification.service';
import { UserService } from '../../../core/services/user.service';
import { ArtItemDTO } from '../../../features/art-items/models/artItem.dto';
import { JwtPayloadDTO } from '../../models/JwtPayload.dto';
import { UserDTO } from '../../models/User.dto';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass']
})
export class UserProfileComponent implements OnInit {
public currUser: UserDTO;
public userArts: ArtItemDTO[];
public loggedUser: JwtPayloadDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: { currUser: UserDTO, arts: ArtItemDTO[] }) => {
        this.currUser = data.currUser;
        this.userArts = data.arts;
      },
      (error) => {
        console.log(error);
      }
    );

    this.authService.loggedUser$.subscribe(
      (user: JwtPayloadDTO) => {
        this.loggedUser = user;
      }
    );
  }

  public getFullName() {
    return `${this.currUser.firstName} ${this.currUser.lastName}`;
  }

  public editUser() {
    this.notificationService.warn('Not implemented yet');
  }

  public deleteUser() {
    this.userService.deleteUser(this.currUser.id).subscribe(
      (res) => {
        console.log(res);
        this.authService.logout()
            this.notificationService.success('Your profile has been removed successfully');
      }
    );
  }

}
