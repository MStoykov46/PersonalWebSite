import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LandingDialogComponent } from './components/landing-dialog/landing-dialog.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ServerErrorPageComponent } from './components/server-error-page/server-error-page.component';
import { UserProfileComponent } from './components/user-profile/user-profile/user-profile.component';
import { CoreModule } from './core/core.module';
import { AttachTokenInterceptor } from './core/interceptors/attachToken-interceptor.service';
import { SpinnerInterceptor } from './core/interceptors/spinner-interceptor.service';
import { ArtItemsModule } from './features/art-items/art-items.module';
import { SharedModule } from './shared/shared.module';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    LandingDialogComponent,
    RegisterComponent,
    ServerErrorPageComponent,
    UserProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          const localSToken = localStorage.getItem('token');
          return localSToken ? localSToken : sessionStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:5000'],
      }
    }),

    SharedModule,
    CoreModule,
    ArtItemsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AttachTokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LandingDialogComponent,
  ]
})
export class AppModule { }
