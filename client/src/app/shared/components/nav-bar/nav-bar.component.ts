import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LandingDialogComponent } from '../../../components/landing-dialog/landing-dialog.component';
import { JwtPayloadDTO } from '../../../components/models/JwtPayload.dto';
import { AuthService } from '../../../core/services/authService.service';
import { DialogService } from '../../../core/services/dialog.service';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.sass']
})
export class NavBarComponent implements OnInit, OnDestroy {
  public isLoggedIn: boolean;
  public user: JwtPayloadDTO;

  private userSub: Subscription;
  private isLoggedSub: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly dialogService: DialogService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.userSub = this.authService.loggedUser$.subscribe(
      (user: JwtPayloadDTO) => {
        this.user = user;
      }
    );

    this.isLoggedSub = this.authService.isLogged$.subscribe(
      (isLogged: boolean) => {
        this.isLoggedIn = isLogged;
        console.log(isLogged);

      }
    );
  }

  ngOnDestroy() {
    this.isLoggedSub.unsubscribe();
    this.userSub.unsubscribe();
  }

  public onLogInSignUpClick() {
    this.dialogService.open(LandingDialogComponent, {});
  }

  public goToCreateArt() {
    this.router.navigateByUrl('/arts/create');
  }

  public logout() {
    this.authService.logout();
  }

  public goToAllArts() {
    this.router.navigateByUrl('/arts');
  }

  public myWorkClick() {
    this.router.navigateByUrl(`arts/my-arts`);
  }

  public goToProfile() {
    this.router.navigateByUrl(`/profile/${this.user.Id}`);
  }

}
