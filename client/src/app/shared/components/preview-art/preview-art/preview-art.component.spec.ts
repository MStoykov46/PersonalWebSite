import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewArtComponent } from './preview-art.component';

describe('PreviewArtComponent', () => {
  let component: PreviewArtComponent;
  let fixture: ComponentFixture<PreviewArtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewArtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewArtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
