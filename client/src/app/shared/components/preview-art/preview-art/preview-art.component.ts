import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ArtItemDTO } from '../../../../features/art-items/models/artItem.dto';

@Component({
  selector: 'app-preview-art',
  templateUrl: './preview-art.component.html',
  styleUrls: ['./preview-art.component.sass']
})
export class PreviewArtComponent {
  @Input() public artItem: ArtItemDTO;
  constructor(
    private readonly router: Router,
  ) { }

    public detailsClick() {
      this.router.navigateByUrl(`/arts/${this.artItem.id}`);
    }
}
