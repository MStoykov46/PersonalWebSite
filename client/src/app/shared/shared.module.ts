import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatMenuModule, MatTabsModule } from '@angular/material';
import { HeaderComponent } from './components/header/header.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PreviewArtComponent } from './components/preview-art/preview-art/preview-art.component';



@NgModule({
  declarations: [
    NavBarComponent,
    HeaderComponent,
    NotFoundComponent,
    PreviewArtComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDialogModule,
    MatIconModule,

    HttpClientModule,
  ],
  exports: [
    HeaderComponent,
    NavBarComponent,
    PreviewArtComponent,
    NotFoundComponent,

    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDialogModule,
    MatIconModule,

    HttpClientModule,
  ]
})
export class SharedModule { }
