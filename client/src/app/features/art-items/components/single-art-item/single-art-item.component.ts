import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtPayloadDTO } from '../../../../components/models/JwtPayload.dto';
import { AuthService } from '../../../../core/services/authService.service';
import { ReviewsService } from '../../../../core/services/reviews.service';
import { ArtItemDTO } from '../../../../features/art-items/models/artItem.dto';
import { ReviewDTO } from '../../../../features/reviews/models/review.dto';

@Component({
  selector: 'app-single-art-item',
  templateUrl: './single-art-item.component.html',
  styleUrls: ['./single-art-item.component.sass']
})
export class SingleArtItemComponent implements OnInit {
  public loggedUser: JwtPayloadDTO;
  public artItem: ArtItemDTO;
  public loadedReviews: ReviewDTO[] = null;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly reviewsService: ReviewsService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (art: ArtItemDTO) => {
          this.artItem = (art as any).art;
      }
    );

    this.authService.loggedUser$.subscribe(
      (loggedUser: JwtPayloadDTO) => {
        this.loggedUser = loggedUser;
      }
    );

  }

  public onCommentsClick() {
    if (!this.loadedReviews) {
      this.reviewsService.getArtReviews(this.artItem.id).subscribe(
        (reviews: ReviewDTO[]) => {
          this.loadedReviews = reviews;
        }
      );
    }
  }

  public goToUserProfile(id: string) {
    this.router.navigateByUrl(`profile/${id}`);
  }

}
