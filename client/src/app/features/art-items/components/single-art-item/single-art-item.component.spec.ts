import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleArtItemComponent } from './single-art-item.component';

describe('SingleArtItemComponent', () => {
  let component: SingleArtItemComponent;
  let fixture: ComponentFixture<SingleArtItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleArtItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleArtItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
