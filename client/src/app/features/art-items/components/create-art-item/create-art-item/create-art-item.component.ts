import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtPayloadDTO } from '../../../../../components/models/JwtPayload.dto';
import { ArtsService } from '../../../../../core/services/arts.service';
import { AuthService } from '../../../../../core/services/authService.service';
import { ArtItemDTO } from '../../../models/artItem.dto';

@Component({
  selector: 'app-create-art-item',
  templateUrl: './create-art-item.component.html',
  styleUrls: ['./create-art-item.component.sass']
})
export class CreateArtItemComponent implements OnInit {
  public createArtItemForm: FormGroup;
  public photo: string = null;
  public loggedUser: JwtPayloadDTO;


  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly artService: ArtsService,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {
  }

  ngOnInit() {
    this.authService.loggedUser$.subscribe(
      (loggedUser: JwtPayloadDTO) => {
        this.loggedUser = loggedUser;
      }
    );
    this.createArtItemForm = this.formBuilder.group({
      description: [
        '',
        [
          Validators.required,
          Validators.minLength(20),
          Validators.maxLength(3000),
        ]
      ],
      image: [
        undefined,
        [
          Validators.required,
        ]
      ]
    });
  }

  public onCreateClick() {
    // console.log(this.createArtItemForm.value);
    this.artService.createArt({ ...this.createArtItemForm.value, userId: this.loggedUser.Id }).subscribe(
      (art: ArtItemDTO) => {
        console.log(art);
        this.router.navigateByUrl(`/arts/${art.id}`);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public onAddPhoto(event: { previewPhoto: string, rawPhoto: File }) {
    this.photo = event.previewPhoto;
    this.createArtItemForm.patchValue({
      image: event.rawPhoto
    });
    this.createArtItemForm.get('image').updateValueAndValidity();
  }

  public onRemovePhoto() {
    this.photo = null;
    this.createArtItemForm.patchValue({
      image: null,
    });
  }

}
