import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.sass']
})
export class UploadImageComponent implements OnInit {
  @Input() public previewUrl: string | ArrayBuffer = undefined;
  @Output() public addPhoto: EventEmitter<{ previewPhoto: string | ArrayBuffer, rawPhoto: File }> = new EventEmitter();
  @Output() public removePhoto: EventEmitter<void> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  photoInputChange(event) {
    const photo: File = event.target.files[0];
    // Show preview
    const mimeType = photo.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(photo);
    reader.onload = _ => {
      this.previewUrl = reader.result;
      this.addPhoto.emit({ previewPhoto: this.previewUrl, rawPhoto: photo});
    };
  }

  removeImg() {
    this.previewUrl = undefined;
    this.removePhoto.emit();
  }
}
