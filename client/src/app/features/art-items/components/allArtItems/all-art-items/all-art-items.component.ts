import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtItemDTO } from '../../../models/artItem.dto';

@Component({
  selector: 'app-all-art-items',
  templateUrl: './all-art-items.component.html',
  styleUrls: ['./all-art-items.component.sass']
})
export class AllArtItemsComponent implements OnInit {
  public arts: ArtItemDTO[];
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (arts) => {
        this.arts = (arts as any).arts;
      }
    );
  }

  public redirectToCreate() {
    this.router.navigateByUrl('/arts/create');
  }

}
