import { Route } from '@angular/router';
import { AuthGuard } from '../../core/guards/auth-guard.service';
import { AllArtsResolver } from '../../core/resolvers/all-arts.resolver';
import { AllUserArtsResolver } from '../../core/resolvers/all-user-arts.resolver';
import { SingleArtResolver } from '../../core/resolvers/single-art.resolver';
import { NotFoundComponent } from '../../shared/components/not-found/not-found.component';
import { AllArtItemsComponent } from './components/allArtItems/all-art-items/all-art-items.component';
import { CreateArtItemComponent } from './components/create-art-item/create-art-item/create-art-item.component';
import { SingleArtItemComponent } from './components/single-art-item/single-art-item.component';

export const routes: Route[] = [
  {
    path: '',
    component: AllArtItemsComponent,
    resolve: { arts: AllArtsResolver }
  },
  {
    path: 'create',
    component: CreateArtItemComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'my-arts',
    component: AllArtItemsComponent,
    resolve: { arts: AllUserArtsResolver },
    canActivate: [AuthGuard],
  },
  {
    path: ':id',
    component: SingleArtItemComponent,
    loadChildren: () => import(`../reviews/reviews.module`).then(m => m.ReviewsModule),
    resolve: { art: SingleArtResolver },
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: NotFoundComponent,
  }
];
