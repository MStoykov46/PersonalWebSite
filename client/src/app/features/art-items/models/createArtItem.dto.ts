export class CreateArtItemDTO {
  description: string;
  image: any;
  userId: string;
}
