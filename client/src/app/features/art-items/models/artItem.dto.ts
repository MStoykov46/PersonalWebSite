import { UserDTO } from '../../../components/models/User.dto';

export class ArtItemDTO {
  id: string;
  description: string;
  image: any;
  userId: string;
  user: UserDTO;
  url: string;
  createTime: string;
}
