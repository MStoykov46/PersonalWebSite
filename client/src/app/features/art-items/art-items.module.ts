import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PreviewArtComponent } from '../../shared/components/preview-art/preview-art/preview-art.component';
import { SharedModule } from '../../shared/shared.module';
import { ReviewsModule } from '../reviews/reviews.module';
import { ArtItemsRoutingModule } from './art-items-routing.module';
import { AllArtItemsComponent } from './components/allArtItems/all-art-items/all-art-items.component';
import { CreateArtItemComponent } from './components/create-art-item/create-art-item/create-art-item.component';
import { SingleArtItemComponent } from './components/single-art-item/single-art-item.component';
import { UploadImageComponent } from './components/upload-image/upload-image.component';



@NgModule({
  declarations: [AllArtItemsComponent, CreateArtItemComponent, UploadImageComponent, SingleArtItemComponent],
  imports: [
    CommonModule,
    SharedModule,
    ArtItemsRoutingModule,
    ReviewsModule,
  ],
  exports: [
    AllArtItemsComponent,
    PreviewArtComponent,
  ]
})
export class ArtItemsModule { }
