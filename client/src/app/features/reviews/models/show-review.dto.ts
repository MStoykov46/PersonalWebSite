export class ShowReviewDTO {

  public id: string;
  public text: string;
  public date: Date;
  public voteUpCount: number;
  public voteDownCount: number;
  public flagsCount: number;
}
