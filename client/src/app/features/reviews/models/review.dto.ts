import { UserDTO } from '../../../components/models/User.dto';

export class ReviewDTO {
  public id: string;
  public content: string;
  public create_time: string;
  public artItemId: string;
  public userId: string;
  public user: UserDTO;
}
