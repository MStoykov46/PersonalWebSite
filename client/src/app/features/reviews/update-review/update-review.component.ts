import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-review',
  templateUrl: './update-review.component.html',
  styleUrls: ['./update-review.component.css']
})
export class UpdateReviewComponent implements OnInit {

  @Input() public reviewTextToUpdate: string;

  public updateReviewForm: FormGroup;

  @Output() closedMenu = new EventEmitter<boolean>();
  @Output() newTextEvent = new EventEmitter<{content: string}>();

  constructor(
    private readonly fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.updateReviewForm = this.fb.group({
      content: [
        `${this.reviewTextToUpdate}`,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(500)
        ])
      ],
    });
  }

  updateReview() {
    this.closedMenu.emit(false);
    this.newTextEvent.emit(this.updateReviewForm.value);
  }

}
