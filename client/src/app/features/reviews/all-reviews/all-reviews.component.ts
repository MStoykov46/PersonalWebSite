import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { JwtPayloadDTO } from '../../../components/models/JwtPayload.dto';
import { AuthService } from '../../../core/services/authService.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ReviewsService } from '../../../core/services/reviews.service';
import { ReviewDTO } from '../models/review.dto';

@Component({
  selector: 'app-all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.css']
})
export class AllReviewsComponent implements OnInit, OnDestroy {

  @Input() public artId: string;
  public loggedUser: JwtPayloadDTO;
  public reviews: ReviewDTO[];


  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.reviewsService.getArtReviews(this.artId).subscribe(
      (res) => {
        this.reviews = res.map(o => {
          o.create_time = moment(o.create_time).fromNow();
          return o;
        });
        console.log(this.reviews);
      }
    );
    this.authService.loggedUser$.subscribe(
      (loggedUser) => {
        this.loggedUser = loggedUser;
      }
    );
  }

  ngOnDestroy(): void {
  }

  updateReview(updatedReview: {reviewId: string, newText: {content: string}}) {
    this.reviewsService.updateReview(updatedReview.reviewId, updatedReview.newText.content).subscribe(
      () => {
        this.notificationService.success(`The review was updated successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  deleteReview(reviewId: string) {
    this.reviewsService.deleteReview(reviewId).subscribe(
      (res) => {
        this.reviews = this.reviews.filter((r) => r.id !== reviewId);
        this.notificationService.success(`${res}`);
      },
      (error) => {
        console.log(error);
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  createReview(newReview: {text: string}) {
    const sub = this.reviewsService.createReview(this.artId, this.loggedUser.Id , newReview.text).subscribe(
      (res: ReviewDTO) => {
        res.create_time = moment(res.create_time).fromNow();
        this.reviews = [res, ...this.reviews];
        this.notificationService.success(`The review was created successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      },
      () => {
        sub.unsubscribe();
      }
    );
  }

}
