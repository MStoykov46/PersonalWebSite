import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { JwtPayloadDTO } from '../../../components/models/JwtPayload.dto';
import { ReviewDTO } from '../models/review.dto';

@Component({
  selector: 'app-review-info',
  templateUrl: './review-info.component.html',
  styleUrls: ['./review-info.component.css']
})
export class ReviewInfoComponent implements OnInit, OnDestroy {

  @Input() review: ReviewDTO;
  @Input() user: JwtPayloadDTO;
  @Output() newTextEvent = new EventEmitter<{reviewId: string, newText: {content: string}}>();
  @Output() deleteReviewEvent = new EventEmitter<string>();
  @Output() newVoteEvent = new EventEmitter<{voteType: string, reviewId: string, operation: string}>();
  @Output() newFlagEvent = new EventEmitter<{flagType: string, reviewId: string}>();
  public canEdit: boolean;
  public isEditMenuOpen = false;

  constructor() { }

  ngOnInit() {
    if ((this.user.username !== this.review.user.username) && this.user.role.name === 'Admin') {
      this.canEdit = false;
    } else {
      this.canEdit = true;
    }
  }

  ngOnDestroy(): void {
  }

  toggleEditMenu() {
    this.isEditMenuOpen = this.isEditMenuOpen ? false : true;
  }

  updateReview(newText: {content: string}) {
    const updatedReview = {
      reviewId: this.review.id,
      newText: {
        ...newText,
      }
    };
    this.review.content = newText.content;
    this.newTextEvent.emit(updatedReview);
  }

  deleteReview() {
    this.deleteReviewEvent.emit(this.review.id);
  }

}
