import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { CreateReviewComponent } from './create-review/create-review.component';
import { ReviewInfoComponent } from './review-info/review-info.component';
import { UpdateReviewComponent } from './update-review/update-review.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    AllReviewsComponent,
    CreateReviewComponent,
    ReviewInfoComponent,
    UpdateReviewComponent,
  ],
  providers: [],
  exports: [AllReviewsComponent]
})
export class ReviewsModule {}

