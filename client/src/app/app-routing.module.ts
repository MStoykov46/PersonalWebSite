import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServerErrorPageComponent } from './components/server-error-page/server-error-page.component';
import { UserProfileComponent } from './components/user-profile/user-profile/user-profile.component';
import { AuthGuard } from './core/guards/auth-guard.service';
import { UserProfileArtsResolver } from './core/resolvers/user-profile-arts.resolver';
import { UserProfileResolver } from './core/resolvers/user-profile.resolver';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';


// canActivate: [AdminGuard],
//     data: { role: 'Admin' },

const routes: Routes = [
  {
    path: '',
    redirectTo: 'arts',
    pathMatch: 'full',
  },
  {
    path: 'home',
    redirectTo: 'arts',
  },
  {
    path: 'profile/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    resolve: { currUser: UserProfileResolver, arts: UserProfileArtsResolver },
  },
  {
    path: 'arts',
    loadChildren: () => import(`./features/art-items/art-items.module`).then(m => m.ArtItemsModule),
  },
  {
    path: 'server-error',
    component: ServerErrorPageComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
