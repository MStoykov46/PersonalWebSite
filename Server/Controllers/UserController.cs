using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Models;
using Server.Models.DTOs;
using Server.Services;
using Microsoft.AspNetCore.Authorization;
using Server.Filters;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService userService { get; set; }
        public UserController(UserService userService)
        {
            this.userService = userService;
        }

        // GET: api/User
        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public async Task<IEnumerable<User>> GetUser()
        {   
            return await userService.getAllUsers();
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(Guid id)
        {
            try {
                return await userService.getUser(id);
            } catch(Exception e) {
                return NotFound(e);
            }
        }

        // PUT: api/User/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<ActionResult<string>> PutUser(Guid id, UpdateUserDTO user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            try {
                return await this.userService.updateUser(user);
            } catch(Exception error) {
                return NotFound(error.Message);
            }
        }

        // POST: api/User
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [AnnonymosOnly]
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(CreateUserDTO user)
        {
            try {
                return await this.userService.registerUser(user);
            } catch (Exception error) {
                return BadRequest(error.Message);
            }
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(Guid id)
        {
            try {
                return await this.userService.deleteUser(id);
            } catch (Exception error) {
                return NotFound(error.Message);
            }
        }

        // POST: api/User/login { username: string, password: string }
        [AnnonymosOnly]
        [HttpPost("login")]
        public async Task<ActionResult<TokenDTO>> LoginUser([FromBody] LoginUserDTO user)
        {
            Console.WriteLine(user);
            try {
                return await this.userService.login(user);
            } catch(Exception error) {
                Console.WriteLine(error.Message);
                return BadRequest(error.Message);
            }
        }
    }
}
