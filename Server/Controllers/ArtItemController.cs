using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Models;
using Server.Models.DTOs;
using Server.Services;
using Microsoft.AspNetCore.Authorization;

namespace Server.Controllers
{
    [Route("api")]
    [Authorize]
    [ApiController]
    public class ArtItemController : ControllerBase
    {

        private ArtItemService artItemService { get; set; }
        public ArtItemController(ArtItemService artItemService)
        {
            this.artItemService = artItemService;
        }

        [AllowAnonymous]
        [HttpGet("arts")]
        public async Task<ActionResult<IEnumerable<ArtItem>>> getAllArts()
        {
            return await this.artItemService.getAllArts();
        }

        [Authorize]
        [HttpPost("art")]
        public async Task<ActionResult<ArtItem>> uploadArtItem([FromForm] CreateArtItemDTO artItem)
        {
            Console.WriteLine(artItem.userId);
            if(artItem.image == null || artItem.image.Length < 1)
            {
                return BadRequest();
            }

            return await this.artItemService.addArtItem(artItem);
        }

        [HttpGet("art/{id}")]
        public async Task<ActionResult<ArtItem>> getSingleArt(Guid id)
        {
            try {
                return await this.artItemService.getById(id);
            } catch (Exception e) {
                return NotFound(e.Message);
            }
        }

        [HttpGet("user/{id}/arts")]
        public async Task<ActionResult<IEnumerable<ArtItem>>> getAllArtsByUser(Guid id)
        {
            return await this.artItemService.getAllArtsByUser(id);
        }

        [Authorize(Roles="ADMIN")]
        [HttpDelete("art/{id}")]
        public async Task<ActionResult<ArtItem>> deleteArt(Guid id)
        {
            return await this.artItemService.deleteArt(id);
        }
    }
}