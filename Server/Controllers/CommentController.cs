using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Models;
using Server.Models.DTOs;
using Server.Services;
using Microsoft.AspNetCore.Authorization;

namespace Server.Controllers
{
    [Route("api")]
    [Authorize]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private CommentService commentService { get; set; }
        public CommentController(CommentService commentService)
        {
            this.commentService = commentService;
        }

        [Authorize]
        [HttpGet("art/{id}/comments")]
        public async Task<ActionResult<IEnumerable<Comment>>> GetUser(Guid id)
        {   
            return await commentService.getAllCommentsForArt(id);
        }

        [Authorize]
        [HttpPost("comments")]
        public async Task<ActionResult<Comment>> addComment([FromBody] CreateCommentDTO comment)
        {
            return await commentService.addComment(comment);
        }

        [Authorize]
        [HttpPut("comments/{id}")]
        public async Task<ActionResult<Comment>> updateComment(Guid id, [FromBody] EditCommentDTO comment)
        {
            try {
                Console.WriteLine(comment.content);
                Console.WriteLine("here");
                return await commentService.editComment(id, comment);
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpDelete("comments/{id}")]
        public async Task<string> deleteComment(Guid id)
        {
            return await commentService.deleteComment(id);
        }
    }
}