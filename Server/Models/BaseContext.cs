using System;
using Microsoft.EntityFrameworkCore;

namespace Server.Models
{
    public class BaseContext : DbContext
    {
        public BaseContext(DbContextOptions<BaseContext> options) : base(options)
        {

        }
        public DbSet<User> User { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<ArtItem> ArtItem { get; set; }
        public DbSet<Comment> Comment { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ArtItem>(e =>
                {
                    e.HasOne(x => x.user);
                    e.HasIndex(x => x.Url).IsUnique();
                }
            );
            
            builder.Entity<Roles>(e =>
                {
                    e.HasIndex(x => x.name).IsUnique();
                    e.HasData(
                        new Roles() {
                            Id = Guid.Parse("68b0da19-ce77-46f5-9a6e-4cac34cad662"),
                            name = "ADMIN"
                        },
                        new Roles() {
                            Id = Guid.Parse("d9f58e42-e346-4fca-98a5-4c1d2765b0d4"),
                            name = "USER"
                        }
                    );
                }
            );

            builder.Entity<User>(e => 
                {
                    e.HasIndex(x => x.create_time);
                    e.HasIndex(x => x.username).IsUnique();
                    e.HasIndex(x => x.email).IsUnique();
                    e.HasData(
                        new User() {
                            Id = Guid.Parse("3f130cc1-7d29-470e-b3bf-048fb335c410"),
                            username = "R2-D2",
                            email = "r2d2@gmail.com",
                            roleId = Guid.Parse("68b0da19-ce77-46f5-9a6e-4cac34cad662"),
                            firstName = "Martin",
                            lastName = "Stoykov",
                            password = BCrypt.Net.BCrypt.HashPassword("1234"),
                            create_time = DateTime.UtcNow,
                        },
                        new User() {
                            Id = Guid.Parse("cd16c407-501f-4d0f-a116-3117a38f31bd"),
                            username = "C-3PO",
                            email = "c3po@gmail.com",
                            roleId = Guid.Parse("68b0da19-ce77-46f5-9a6e-4cac34cad662"),
                            firstName = "NotMartin",
                            lastName = "NotStoykov",
                            password = BCrypt.Net.BCrypt.HashPassword("1234"),
                            create_time = DateTime.UtcNow,
                        }
                    );
                }
            );
        }
    }
}