using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string username { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [ForeignKey("Roles")]
        public Guid roleId { get; set; }
        public virtual Roles role { get; set; }
        [Required]
        [MaxLength(50)]
        public string firstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string lastName { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public DateTime create_time { get; set; } = DateTime.UtcNow.ToLocalTime();
    }
}