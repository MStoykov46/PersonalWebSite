using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.Models
{
    public class ArtItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey("User")]
        public Guid userId { get; set; }
        public virtual User user { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public DateTime create_time { get; set; } = DateTime.UtcNow;
    }
}