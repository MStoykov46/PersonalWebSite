using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [ForeignKey("ArtItem")]
        public Guid artItemId { get; set; }
        public virtual ArtItem artItem { get; set; }
        [ForeignKey("User")]
        public Guid userId { get; set; }
        public virtual User user { get; set; }
        [Required]
        [MinLength(5)]
        public string content { get; set; }
        [Required]
        public DateTime create_time { get; set; } = DateTime.UtcNow.ToLocalTime();
    }
}