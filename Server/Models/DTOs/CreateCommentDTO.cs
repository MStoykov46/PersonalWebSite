using System;

namespace Server.Models.DTOs
{
    public class CreateCommentDTO
    {
        public Guid artItemId { get; set; }
        public Guid userId { get; set; }
        public string content { get; set; }

    }
}