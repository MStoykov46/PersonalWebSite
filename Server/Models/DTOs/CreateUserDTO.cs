using System.ComponentModel.DataAnnotations;

namespace Server.Models.DTOs
{
    public class CreateUserDTO
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string password { get; set; }
    }
}