namespace Server.Models.DTOs
{
    public class TokenDTO
    {
        public string token { get; set; }
    }
}