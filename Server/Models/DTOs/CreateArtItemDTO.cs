using Microsoft.AspNetCore.Http;
using System;

namespace Server.Models.DTOs
{
    public class CreateArtItemDTO
    {
        public string description { get; set; }
        public IFormFile image { get; set; }
        public Guid userId { get; set; }
    }
}