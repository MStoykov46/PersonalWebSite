using System;

namespace Server.Models.DTOs
{
    public class UpdateUserDTO
    {
        public Guid Id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public Guid roleId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}