using System;

namespace Server.Models
{
    public class UserViewDTO
    {
        public Guid Id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public Roles role { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string create_time { get; set; }
    }
}