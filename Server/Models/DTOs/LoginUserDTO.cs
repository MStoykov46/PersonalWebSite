using System.ComponentModel.DataAnnotations;

namespace Server.Models.DTOs
{
    public class LoginUserDTO
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}