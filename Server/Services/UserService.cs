using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Models;
using Server.Models.DTOs;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Server.Helpers;
using System.Text.Json;

namespace Server.Services
{
    public class UserService
    {
        private readonly BaseContext _context;
        private readonly AppSettings _appSettings;

        public UserService(BaseContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public async Task<IEnumerable<User>> getAllUsers()
        {
            var users = await _context.User.Include(x => x.role).ToListAsync(); 
            
            return users.WithoutPasswords();
        }

        public async Task<ActionResult<User>> getUser(Guid id)
        {
            var user = await _context.User.Include(x => x.role).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new Exception();
            }

            return user.WithoutPassword();
        }

        public async Task<ActionResult<string>> updateUser(UpdateUserDTO user)
        {
            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return "User updated successful";
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    throw new Exception();
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task<ActionResult<User>> registerUser(CreateUserDTO user)
        {
            if (_context.User.Where(x => x.username == user.username || x.email == user.email).FirstOrDefault() != null)
            {
                throw new Exception();
            }
            var userRole = await _context.Roles.FirstOrDefaultAsync(x => x.name == "USER");
            var userToCreate = new User()
            {
                username = user.username,
                email = user.email,
                firstName = user.firstName,
                lastName = user.lastName,
                password = BCrypt.Net.BCrypt.HashPassword(user.password),
                roleId = userRole.Id,
            };

            var entry = _context.User.Add(userToCreate);
            await _context.SaveChangesAsync();

            return userToCreate.WithoutPassword();
        }

        public async Task<ActionResult<User>> deleteUser(Guid id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                throw new Exception();
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return user.WithoutPassword();
        }

        public async Task<ActionResult<TokenDTO>> login(LoginUserDTO user)
        {
            var logInUser = await _context.User.Include(x => x.role).SingleOrDefaultAsync(x => x.username == user.username);
            Console.WriteLine(logInUser.username);
            if (logInUser == null)
            {
                return null;
            }

            if (!BCrypt.Net.BCrypt.Verify(user.password, logInUser.password))
            {
                throw new Exception("Password does not match");
            }

            try {
                var key = _appSettings.Secret;
                var userPayload = new UserViewDTO() {
                    Id = logInUser.Id,
                    username = logInUser.username,
                    email = logInUser.email,
                    role = logInUser.role,
                    firstName = logInUser.firstName,
                    lastName = logInUser.lastName,
                    create_time = logInUser.create_time.ToString(),
                };
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("user", JsonSerializer.Serialize(userPayload)),
                    new Claim(ClaimTypes.Role, logInUser.role.name)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = new TokenDTO()
            {
                token = tokenHandler.WriteToken(jwtToken),
            };
            return token;
            } catch(Exception e) {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
            
        }


        private bool UserExists(Guid id)
        {
            return _context.User.Any(e => e.Id == id);
        }

        
    }
}