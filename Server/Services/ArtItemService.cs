using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Models.DTOs;
using Server.Models;

namespace Server.Services
{
    public class ArtItemService
    {
        private readonly BaseContext _context;

        public ArtItemService(BaseContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<ArtItem>> addArtItem(CreateArtItemDTO artItem)
        {
            var folderName = Path.Combine("Resources", "Arts");
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            var uniqueFileName = $@"{Guid.NewGuid()}.png";
            var dbPath = Path.Combine(folderName, uniqueFileName);

            using (var fileStream = new FileStream(Path.Combine(filePath, uniqueFileName), FileMode.Create))
            {
                await artItem.image.CopyToAsync(fileStream);
            }

            var artToSave = new ArtItem()
            {
                userId = artItem.userId,
                description = artItem.description,
                Url = dbPath
            };

            var entry = _context.Add(artToSave);
            await _context.SaveChangesAsync();

            var artItemToReturn = await _context.ArtItem.Include(x => x.user).ThenInclude(it => it.role).FirstOrDefaultAsync(x => x.Url == dbPath);

            return artItemToReturn;
        }

        public async Task<ActionResult<IEnumerable<ArtItem>>> getAllArts()
        {
            var arts = await _context.ArtItem.Include(x => x.user).ThenInclude(it => it.role).ToListAsync();
            arts.Select(x => {
                x.user.password = null;
                return x;
            });
            return arts;
        }

        public async Task<ActionResult<ArtItem>> getById(Guid id)
        {
            var artItem = await _context.ArtItem.Include(x => x.user).ThenInclude(it => it.role).FirstOrDefaultAsync(x => x.Id == id);
            artItem.user.password = null;
            return artItem;
        }

        public async Task<ActionResult<IEnumerable<ArtItem>>> getAllArtsByUser(Guid userId)
        {
            return await _context.ArtItem.Include(x => x.user).ThenInclude(it => it.role).Where(x => x.userId == userId).ToListAsync();
        }

        public async Task<ActionResult<ArtItem>> deleteArt(Guid id)
        {
            var artItem = await _context.ArtItem.FindAsync(id);
            if (artItem == null)
            {
                throw new Exception();
            }

            _context.ArtItem.Remove(artItem);
            await _context.SaveChangesAsync();

            return artItem;
        }
    }
}