using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Models;
using Server.Models.DTOs;

namespace Server.Services
{
    public class CommentService
    {
        private readonly BaseContext _context;

        public CommentService(BaseContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<Comment>> addComment(CreateCommentDTO newComment)
        {
            var createdComment = new Comment()
            {
                content = newComment.content,
                userId = newComment.userId,
                artItemId = newComment.artItemId,
            };
            var entity = _context.Add(createdComment);
            await _context.SaveChangesAsync();
        
            return await _context.Comment.Include(x => x.user).ThenInclude(it => it.role).FirstAsync(x => x.Id == createdComment.Id);
        }

        public async Task<ActionResult<IEnumerable<Comment>>> getAllCommentsForArt(Guid artItemId)
        {
            var comments = await _context.Comment.Include(x => x.user).ThenInclude(it => it.role).Where(x => x.artItemId == artItemId).ToListAsync();
            comments.Select(x => {
                x.user.password = null;
                return x;
            });

            return comments;
        }

        public async Task<ActionResult<Comment>> editComment(Guid id, EditCommentDTO comment)
        {
            Console.WriteLine(id);

            Console.WriteLine(comment.content);

            var commentToEdit = await _context.Comment.FirstOrDefaultAsync(x => x.Id == id);
            if (commentToEdit == null)
            {
                throw new Exception();
            }
            commentToEdit.content = comment.content;
            _context.Comment.Update(commentToEdit);
            await _context.SaveChangesAsync();
            return await _context.Comment.Include(x => x.user).ThenInclude(it => it.role).FirstOrDefaultAsync(y => y.Id == commentToEdit.Id);
        }

        public async Task<string> deleteComment(Guid id)
        {
            var commentToDelete = await _context.Comment.FirstOrDefaultAsync(x => x.Id == id);
            if(commentToDelete == null)
            {
                throw new Exception();
            }
            _context.Remove(commentToDelete);
            await _context.SaveChangesAsync();
            return "Success";
        } 
    }
}