using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Server.Filters
{
    public class AnnonymosOnlyFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string authHeader = context.HttpContext.Request.Headers["Authorization"];
            
            if (authHeader != "Bearer null")
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}