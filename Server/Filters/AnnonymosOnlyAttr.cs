using Microsoft.AspNetCore.Mvc;
using System;

namespace Server.Filters
{
    public class AnnonymosOnlyAttribute : TypeFilterAttribute
    {
        public AnnonymosOnlyAttribute() : base(typeof(AnnonymosOnlyFilter))
        {
        }
    }
}