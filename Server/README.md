.Net Core 3.1 and MySQL with Workbench GUI

# Documentation:
Go to http://localhost:5000/swagger


1. Create db schema
2. Add your data in the connection string inside `appsettings.json`
3. Run `dotnet ef migration add 'Name'`
4. Run `dotnet ef database update` /It should create the tables/
5. Run `dotnet build`
6. Run `dotnet run`