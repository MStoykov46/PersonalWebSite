﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Server.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    username = table.Column<string>(maxLength: 50, nullable: false),
                    email = table.Column<string>(nullable: false),
                    roleId = table.Column<Guid>(nullable: false),
                    firstName = table.Column<string>(maxLength: 50, nullable: false),
                    lastName = table.Column<string>(maxLength: 50, nullable: false),
                    password = table.Column<string>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Roles_roleId",
                        column: x => x.roleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArtItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    userId = table.Column<Guid>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    Url = table.Column<string>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArtItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArtItem_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    artItemId = table.Column<Guid>(nullable: false),
                    userId = table.Column<Guid>(nullable: false),
                    content = table.Column<string>(nullable: false),
                    create_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_ArtItem_artItemId",
                        column: x => x.artItemId,
                        principalTable: "ArtItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comment_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("68b0da19-ce77-46f5-9a6e-4cac34cad662"), "ADMIN" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("d9f58e42-e346-4fca-98a5-4c1d2765b0d4"), "USER" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "create_time", "email", "firstName", "lastName", "password", "roleId", "username" },
                values: new object[] { new Guid("3f130cc1-7d29-470e-b3bf-048fb335c410"), new DateTime(2020, 1, 31, 11, 39, 34, 884, DateTimeKind.Utc).AddTicks(9110), "r2d2@gmail.com", "Martin", "Stoykov", "$2a$11$NQuaegkXQOTqpOch4QNrLOrmVxXRE.kOTVjfMdR.TKpRwWX92b54m", new Guid("68b0da19-ce77-46f5-9a6e-4cac34cad662"), "R2-D2" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "create_time", "email", "firstName", "lastName", "password", "roleId", "username" },
                values: new object[] { new Guid("cd16c407-501f-4d0f-a116-3117a38f31bd"), new DateTime(2020, 1, 31, 11, 39, 35, 102, DateTimeKind.Utc).AddTicks(1690), "c3po@gmail.com", "NotMartin", "NotStoykov", "$2a$11$Hr125WGxJ0T.eQT5oO1q0Opr8XRDRL1PFuVoe0Jag1XEd.4AR4df2", new Guid("68b0da19-ce77-46f5-9a6e-4cac34cad662"), "C-3PO" });

            migrationBuilder.CreateIndex(
                name: "IX_ArtItem_Url",
                table: "ArtItem",
                column: "Url",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ArtItem_userId",
                table: "ArtItem",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_artItemId",
                table: "Comment",
                column: "artItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_userId",
                table: "Comment",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_name",
                table: "Roles",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_create_time",
                table: "User",
                column: "create_time");

            migrationBuilder.CreateIndex(
                name: "IX_User_email",
                table: "User",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_roleId",
                table: "User",
                column: "roleId");

            migrationBuilder.CreateIndex(
                name: "IX_User_username",
                table: "User",
                column: "username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "ArtItem");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
